
ifdef  CONFIG_EXTERNAL_BOOST

# Targets provided by this project
.PHONY: boost clean_boost

# Add this to the "external" target
external: boost
clean_external: clean_boost

MODULE_DIR_BOOST=external/required/boost



################################################################################
# Generic-Linux Target Build Procedure
################################################################################
ifdef CONFIG_TARGET_LINUX

# Machine type: 32 or 64 bit architecture?
ifndef CONFIG_TARGET_LINUX_ARM
ARCH=64
ifeq (,$(findstring x86_64,$(PLATFORM_TARGET)))
	ifneq (,$(findstring i386,$(PLATFORM_TARGET)))
		ARCH=32
	else ifeq (,$(findstring 32,$(PLATFORM_TARGET)))
		ARCH=32
	else
		echo "Undefined platform target"
		ARCH=0
	endif
endif
endif #ARM

ifdef CONFIG_TARGET_LINUX_ARM
	GCC_VERSION=bosp
endif #ARM

boost: setup $(BUILD_DIR)/lib/libboost_filesystem.a
$(BUILD_DIR)/lib/libboost_filesystem.a:
	@echo
	@echo "==== Building Boost librararies v1.55.0 ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " Machine type : $(PLATFORM_TARGET)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	# Check-out the proper branch
	@cd $(MODULE_DIR_BOOST) && \
	git reset --hard HEAD && \
	git checkout bosp
	# TODO: before building, clean-up unused libraries
	@cd $(MODULE_DIR_BOOST)/ && \
		echo using gcc : $(GCC_VERSION) : $(CXX) : $(TARGET_FLAGS) \; > \
		tools/build/v2/user-config.jam && \
	CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
	CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
	./bootstrap.sh \
		--without-icu --prefix=$(BUILD_DIR) \
		--with-libraries=system,program_options,filesystem,regex,serialization ||  \
		exit 1 && \
	./b2 \
		address-model=$(ARCH) \
		toolset=gcc-$(GCC_VERSION) -a -q -d0 -j$(shell [ $(CPUS) -gt 64 ] && echo 64 || echo $(CPUS)) \
		cxxflags="$(TARGET_FLAGS) -fexceptions -frtti -Wno-unused-local-typedefs" install
	@touch $(BUILD_DIR)/lib/libboost_filesystem.a
	@echo

clean_boost:
	@echo "==== Clean-up Boost libraries v1.55.0 ===="
	@cd $(MODULE_DIR_BOOST)/ && ./b2 clean
	@cd $(MODULE_DIR_BOOST)/ && git reset --hard HEAD
	@cd $(MODULE_DIR_BOOST)/ && rm -rf `cat .gitignore`
	@rm -rf $(BUILD_DIR)/lib/libboost_*
	@echo

endif # CONFIG_TARGET_LINUX

################################################################################
# Android-Linux Target Build Procedure
################################################################################
ifdef CONFIG_TARGET_ANDROID

ANDROID_NDK_DIR := $(BASE_DIR)/external/tools/android-ndk
ANDROID_CXX_DIR := $(ANDROID_NDK_DIR)/toolchains/arm-linux-androideabi-4.6.3/prebuilt/linux-x86/bin

boost: android-ndk $(BUILD_DIR)/lib/libboost_filesystem.a
$(BUILD_DIR)/lib/libboost_filesystem.a:
	@echo
	@echo "==== Building Android-Boost librararies v1.45.0 ===="
	# Check-out the proper branch
	@cd $(MODULE_DIR_BOOST) && \
	git reset --hard HEAD && \
	git checkout bosp_android && \
	./build-android.sh \
		$(BUILD_DIR) $(ANDROID_NDK_DIR) $(CPUS)
	@echo

clean_boost:
	@echo "==== Clean-up Android-Boost libraries v1.45.0 ===="
	@cd $(MODULE_DIR_BOOST) && \
		PATH="$(ANDROID_CXX_DIR):$$PATH" \
		./bjam toolset=gcc-bosp clean
	@rm -rf $(BUILD_DIR)/lib/libboost_*
	@rm -rf $(BUILD_DIR)/include/boost/
	@echo

endif # CONFIG_TARGET_ANDROID

else # CONFIG_EXTERNAL_BOOST

boost:
	$(warning $(MODULE_DIR_BOOST) library disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_BOOST
